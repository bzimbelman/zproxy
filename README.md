# zproxy

## Welcome

ZProxy is a proxy server that also assists in debugging applications. It allows you to develop your web or mobile application while proxying the server connections on your laptop computer. These connections can then be routed to different backends through various vpn or other connections and you can easily switch the connection you have and re-run a scenario then compare the results from different backends. This makes it easy to compate the results you are getting in production and in staging for example.

## UI to manage ZProxy

The UI to manage ZProxy is called ZProxymon, it will load by default on port 3000 of your proxy host. Navigate to it and you can configure as many proxies as your application will need. Then you will no longer have to manage a bunch of environment variables to switch your application from a local backend, to your development environment or to production. Simple toggle the proxies from one environment to another.

ZProxymon will also record and allow you to view network traffic through the proxy as well as logging messages and if your state library is connected correctly your state updates. Other monitoring tools can be integrated into it leading to a single point of monitoring that can be captured and reviewed to determine how each environment performed differently. It can be any combination of an issue tracker, a chat room, an email address, etc.

# API Interface

POST proxy/create - creates a proxy based on a configuration object passed in (json format). returns the proxy's id.
POST proxy/log-start/:id - informs proxy to start logging all information to a file on the server (filename is passed in) so that the results can be used later.
POST proxy/log-stop/:id - informs proxy to stop logging and store the contents of the log file.
DELETE proxy:id - removes the proxy.
GET proxy/log/:id - Gets the log for the proxy.

# Web Interface

Displays the results of each network activity, including it's status, time it took to process if completed, results if completed.

# docker setup

A docker-compose (or similar) script will be provided here to start the proxy for a configuration (BMD for example) on your local machine. It will give you the option of changing the ports used, with reasonable defaults that you can use with that client to manage your debugging and running of your app.

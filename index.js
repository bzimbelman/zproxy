const proxyServer = require("./proxyServer");
const controlServer = require("./controlServer");
const fs = require("fs");
const { fork } = require("child_process");
const clc = require("cli-color");

const controlFile = process.env.CONTROL_FILE || "./startDefaultProxy.json";
console.log(`CONTROL FILE: ${controlFile}`);
const jsonString = fs.readFileSync(controlFile, "utf8");
const control = JSON.parse(jsonString);
const controlConfig = control?.control;
const controlPort = controlConfig?.port || 8886;
const wsPort = controlConfig?.wsPort || controlPort + 1;
console.log(`controlPort ${controlPort}, wsPort ${wsPort}`);

controlServer.createControlServer();
controlServer.startControlServer(controlPort, wsPort);

const proxies = control?.proxies;
proxies.forEach((p) => {
  console.log(`Starting child process proxy for: `, p);
  const proxyProcess = fork("./proxyProcess", [JSON.stringify(p)]);

  proxyProcess.on("message", (m) => {
    const msg = m?.message;
    if (m?.type === "NETWORK" || m?.type === "CALLED" || m?.type === "LOG") {
      // console.log(`CODE: ${msg?.code}`);
      // console.log(`${m?.type}:${msg?.id}-SEND MSG TO websocket for : `, m);
      if (msg?.method === "OPTIONS") return;
      if (msg?.code == null) {
        console.log(clc.green(`${m?.type}:${msg?.id}:${msg?.time}:${msg?.method}:${msg?.url}`));
      } else {
        const clcMethod = msg?.code < 400 ? clc.blueBright : clc.red;
        console.log(
          clcMethod(`${m?.type}:${msg?.id}:${msg?.time}:${msg?.method}:${msg?.url}:${msg?.code}:${msg?.body}`)
        );
      }
      controlServer.sendToClients(m?.type, m?.message);
    } else {
      console.log(`Received message of unknown type: `, m);
    }
  });

  process.on("close", (code) => {
    console.log(`child process closed with ${code}...`);
  });

  process.on("exit", (code, signal) => {
    console.log(`child exited via signal ${signal} with ${code}...`);
  });

  process.on("error", (err) => {
    console.log(`Encountered error with child process: `, err);
  });
});

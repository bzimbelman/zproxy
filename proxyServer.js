const http = require("http");
const httpProxy = require("http-proxy");
const zlib = require("zlib");

var server = undefined;
var config = undefined;
var documentationFunction = undefined;

const defaultConfig = {
  port: 8887,
  id: "platform",
  logLevel: "standard",
  target: {
    baseUrl: "https://platform-api.dev-ue1.icanbwell.com",
    headers: [
      { header: "origin", value: "https://portal.dev.icanbwell.com" },
      { header: "referrer", value: "https://portal.dev.icanbwell.com" },
    ],
  },
};

// const sendToClients = (message) => {
//   console.log(`SEND TO CLIENTS: `, message);
//   webServer?.clients?.forEach((client) => {
//     if (client.readyState === WebSocket.OPEN) {
//       console.log(`Sending...`);
//       client.send(JSON.stringify({ type: "NETWORK", content: message }));
//     }
//   });
// };

const createProxyServer = (conf) => {
  config = conf || defaultConfig;
  const proxy = httpProxy.createProxyServer({ rejectUnauthorized: false });
  var uniqueId = 0;

  const processRequest = (req) => {
    const message = {
      name: config?.id,
      id: req?.control?.id,
      time: new Date(),
      method: req?.method,
      url: req?.url,
    };
    if (config.logLevel !== "none") {
      // console.log(`PROXY: `, message);
      documentationFunction("CALLED", message);
    }
  };

  const processResult = (code, msg, control, body) => {
    const logLevel = config?.logLevel;
    const message = {
      name: config?.id,
      id: control?.id,
      time: new Date(),
      method: control?.method,
      url: control?.url,
      code: code,
      message: msg,
      body: logLevel === "verbose" ? body : "",
    };
    if (config.logLevel !== "none") {
      // console.log(`PROXY: `, message);
      documentationFunction("NETWORK", message);
    }
    // if (config.logLevel === "standard") {
    //   // console.log(
    //   //   `PROXY: { name: ${message.name}, id: ${message.id}, method: ${message.method}, url: ${message.url}, responseCode: ${message.code} }`
    //   // );
    //   documentationFunction("NETWORK", message);
    // }
  };

  const processError = (code, msg, error, control, body) => {
    const message = {
      name: config?.id,
      id: control?.id,
      method: control?.method,
      url: control?.url,
      code: code,
      message: msg,
      error: error,
      body: body,
    };
    if (config.logLevel !== "none") {
      // console.log(`PROXY: `, message);
      documentationFunction("NETWORK", message);
    }
    // if (config.logLevel === "standard") {
    //   // console.log(
    //   //   `PROXY: { name: ${message.name}, id: ${message.id}, method: ${message.method}, url: ${message.url}, responseCode: ${message.code}, error: ${message.error} }`
    //   // );
    //   documentationFunction("NETWORK", message);
    // }
  };

  const enableCors = (req, res) => {
    if (req.headers["access-control-request-method"]) {
      res.setHeader("access-control-allow-methods", req.headers["access-control-request-method"]);
    }

    if (req.headers["access-control-request-headers"]) {
      res.setHeader("access-control-allow-headers", req.headers["access-control-request-headers"]);
    }

    if (req.headers.origin) {
      res.setHeader("access-control-allow-origin", req.headers.origin);
      res.setHeader("access-control-allow-credentials", "true");
    }
  };

  proxy.on("proxyRes", function (proxyRes, req, res) {
    var bodyBuffers = [];
    proxyRes.on("data", function (chunk) {
      bodyBuffers.push(chunk);
    });
    proxyRes.on("end", function () {
      const headers = proxyRes?.headers;
      const contentEncoding = headers["content-encoding"];

      if (contentEncoding?.includes("gzip")) {
        zlib.gunzip(Buffer.concat(bodyBuffers), (err, dezipped) => {
          processResult(proxyRes?.statusCode, proxyRes.statusMessage, req?.control, dezipped.toString());
        });
      } else {
        processResult(
          proxyRes?.statusCode,
          proxyRes.statusMessage,
          req?.control,
          Buffer.concat(bodyBuffers).toString()
        );
      }
    });
    proxyRes.on("error", function (err) {
      processError(proxyRes?.statusCode, proxyRes?.statusMessage, req?.control, err, body);
    });
  });
  proxy.on("proxyReq", function (proxyReq, req, res) {
    enableCors(req, res);
    // console.log(
    //   `REQ: ${req?.method} ${req?.url} ${proxyReq?.method} ${proxyReq?.protocol}//${proxyReq?.host}${proxyReq?.path}`
    // );
    // console.log(`REQ Headers: `, req?.headers);
    const headers = config?.target?.headers;
    headers.forEach((h) => {
      proxyReq.setHeader(h.header, h.value);
      // console.log(`PROXY setHeader ${h.header} ${h.value}`);
    });
    proxyReq.setHeader(
      "user-agent",
      "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36"
    );
  });
  proxy.on("error", function (err, req, res) {
    console.log(`PROXY: error caught: `, err);
    res.writeHead(500, {
      "Content-Type": "text/plain",
    });
    res.end(`Something went wrong on ${req?.method} ${req?.url} response: ${res?.statusCode}`, err);
  });

  server = http.createServer(async function (req, res) {
    req.control = { method: req?.method, url: req?.url, id: uniqueId++ };
    processRequest(req);

    if (req.method === "OPTIONS") {
      // console.log(`enabling cors requests`);
      enableCors(req, res);
      res.writeHead(200);
      res.end();
      return;
    }

    proxy.web(req, res, {
      target: config.target.baseUrl,
      selfHandleResponse: false,
      changeOrigin: true,
    });
  });
};

const startProxyServer = (port) => {
  console.log(`PROXY: Server starting on ${port}`);
  server.listen(port);
};

const setDocumentationFunction = (df) => {
  documentationFunction = df;
};

module.exports = { createProxyServer, startProxyServer, setDocumentationFunction };

const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
const WebSocket = require("ws");
const clc = require("cli-color");

var wss = null;

app.use(cors());
app.use(bodyParser.json());

const colorLevels = {
  10: clc.black,
  20: clc.black,
  30: clc.green,
  40: clc.magenta,
  50: clc.red,
};

const levelNames = {
  10: "trace",
  20: "debug",
  30: "info",
  40: "warn",
  50: "error",
  60: "fatal",
};

const log = (level, ...args) => {
  const clcMethod = colorLevels[`${level}`];
  console.log(clcMethod(args));
};

const createControlServer = () => {
  app.post("/log", (req, res) => {
    const body = req.body;
    // console.log(`received log msg: `, body);
    if (Array.isArray(body)) {
      body.forEach((m) => {
        log(m?.level, `${m?.name}[${m?.levelName}]:${m?.time}: ${m?.msg}`);
      });
    } else {
      const levelName = levelNames[body?.level] || "unknown";
      log(body?.level, `${body?.name}[${levelName}]:${body?.time}: ${body?.msg}`);
    }
    res.send(`control received log request...`);
    wss?.clients?.forEach((client) => {
      if (client.readyState === WebSocket.OPEN) {
        client.send(JSON.stringify({ type: "LOG", content: body }));
      }
    });
  });

  app.post("/api", (req, res) => {
    res.send(`control received command...`);
  });
};

const startControlServer = (controlPort, wsPort) => {
  app.listen(controlPort, () => {
    console.log(`express is active on ${controlPort}`);

    wss = new WebSocket.Server({ port: wsPort });
    // console.log(`wss: `, wss);

    wss.on("connection", (ws) => {
      console.log(`Established ws connection`);
      ws.on("message", (message) => {
        console.log(`WS Message: `, message.toString());
      });
    });
  });
};

const sendToClients = (type, message) => {
  wss?.clients?.forEach((client) => {
    if (client.readyState === WebSocket.OPEN) {
      client.send(JSON.stringify({ type: type, content: message }));
    }
  });
};

module.exports = { createControlServer, startControlServer, sendToClients };

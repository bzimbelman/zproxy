const proxyServer = require("./proxyServer");

const proxyConfig = JSON.parse(process.argv[2]);
// console.log(`proxy child: `, proxyConfig);
// const verbosity = proxyConfig?.logLevel;

process.on("message", (m) => {
  console.log(`proxy process received ${m}`);
  process.send(`child process message ${m} for ${proxyConfig.id} on ${proxyConfig.port}`);
});

const document = (type, message) => {
  // console.log(`DOCUMENT ${type} ${proxyConfig?.logLevel}: `, message);
  process.send({ type, message });
};

proxyServer.createProxyServer(proxyConfig);
proxyServer.startProxyServer(proxyConfig.port);
proxyServer.setDocumentationFunction(document);
// proxyServer.setControlServer(controlServer);

module.exports = { document };
